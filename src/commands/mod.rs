mod clear;
mod info;
mod nsfw;
mod roles;
mod shutdown;

// Export the commands
pub use self::clear::clear as clear_command;
pub use self::clear::sclear as sclear_command;
pub use self::clear::uclear as uclear_command;
pub use self::info::info as info_command;
pub use self::nsfw::nsfw as nsfw_command;
pub use self::roles::{roles_add, roles_list, roles_remove};
pub use self::shutdown::shutdown as shutdown_command;
