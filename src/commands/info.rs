use crate::BOT_CONFIGURATION;

// Send info message
command!(info(ctx, message) {
    // Retrieve channel_id
    let channel_id = message.channel_id;
    // Lock the context data
    if let Some(data_lock) = ctx.data.try_lock() {
        // Create message
        let _ = channel_id.send_message(|m| {
                    m.embed(|e| {e
                        .title("Info")
                        .description("A private bot for the mint_gaming network.")
                        .field("Command Prefix", &BOT_CONFIGURATION.command_prefix, true)
                        .field("Language", "Rust", true)
                        .color((217, 217, 67))
                    })
                });
    }
    
});
