use std::process;
use std::{thread, time};

// Bot shutdown
command!(shutdown(ctx, message) {
    if let Some(channel) = message.channel() {
        let channel_id = channel.id();
        // Create message
        let _ = channel_id.send_message(|m| {
            m.embed(|e| {
                e.title("Shutdown")
                .description("Shutdown initiated.")
                .color((255, 0, 0))
            })
        });
        // Let the bot disappear in the moment the command is called
        ctx.invisible();

        // Let the thread sleep for a second so that the packet is already sent
        let duration = time::Duration::from_secs(1);
        thread::sleep(duration);
        // Stop the bot
        ctx.quit();
        process::exit(0);
    }
});
