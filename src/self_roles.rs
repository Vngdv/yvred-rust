use std::collections::HashMap;
use typemap::Key;

pub struct SelfRoles {
    // Contains the MessageId types converted to u64
    pub message_ids: Vec<u64>,
    // Contains the EmojiId type converted to u64 as a key and the RoleId as a value
    pub emoji_roles: HashMap<u64, u64>,
}

impl Key for SelfRoles {
    type Value = Self;
}

impl SelfRoles {
    pub fn new() -> SelfRoles {
        SelfRoles {
            message_ids: Vec::new(),
            emoji_roles: HashMap::new(),
        }
    }
}
